import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ElementService } from '../services/element.service';
import { element } from '../interfaces/element.interface';
import { CardComponent } from '../components/card/card.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild('cardComponent') cardComponent:CardComponent;

  public data: Array<element> = [];
  public dataPaginated: Array<any> = [];
  public dataSearch: Array<any> = [];
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public elementSearch: string = '';

  constructor(private elementSrv: ElementService) { }

  ngOnInit() {

    console.log('Prueba de datos', this.elementSrv.getData());
    this.data = this.elementSrv.getData();
    this.paginateData(this.data, 10, 1);
  }

  paginateData(data: Array<element>, pageSize: number, pageNumber: number) {

    let elements = [];
    --pageNumber;
    elements = this.data.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize);

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < elements.length; i++) {
      this.dataPaginated.push(elements[i]);
    }

  }

  doInfinite(event: any) {

    setTimeout(() => {
      console.log('InfiniteScroll', event);
      this.paginateData(this.data, this.pageSize, this.pageNumber++);
      event.target.complete();
    }, 500);

  }

  resetElementSearch() {
    this.elementSearch = '';
  }

  search() {
    const found = this.data.filter(element => element.id === this.elementSearch || element.text.includes(this.elementSearch));
    this.dataSearch = found;
    console.log(this.dataSearch);
  }

}
