import { Injectable } from '@angular/core';
import { element } from '../interfaces/element.interface';
import { numberItems } from '../constants/constants';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class ElementService {

  data: Array<element> = [];
  id: number = 1;

  constructor(private utilsSrv: UtilsService) { }

  getData(): Array<element> {
    for (let i = 0; i < numberItems; i++) {

      this.data.push({ id: this.id.toString(), photo: this.utilsSrv.getUrlPhoto(this.id), text: this.utilsSrv.getRandomText() });

      this.id++;

    }

    return this.data;

  }
}
