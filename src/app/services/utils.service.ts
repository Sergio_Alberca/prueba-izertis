import { Injectable } from '@angular/core';
import { apiUrl, randomText, characters } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  getUrlPhoto(id: number): string {

    return apiUrl + '/' + id.toString() + '/500/500';

  }

  getRandomText(): string {

    let result = '';
    let charactersLength = characters.length;
    let length = 200;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;

  }
}
